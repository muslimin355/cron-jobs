const cron = require('node-cron');
const axios = require('axios');
const dotenv = require('dotenv');
dotenv.config();

//read po confirmation
cron.schedule('0/5 * * * * *', function() {
    axios.post(process.env.APP_API_URL+'/api/sftp/read-confirmation-po')
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
});

//invoice subsmission
cron.schedule('0/5 * * * * *', function() {
    axios.post(process.env.APP_API_URL+'/api/sftp/read-invoice-submission')
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
});


//get error respons
cron.schedule('0 13 * * * *', function() {
    axios.get(process.env.APP_API_URL+'/api/sftp/read-error-response')
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
});


//delivery order
cron.schedule('0/5 * * * * *', function() {
    axios.post(process.env.APP_API_URL+'/api/sftp/read-delivery-order')
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
});

